#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
from math import atan2, sqrt

class TeleopCoord(object):
    def __init__(self):
        # Inicializar el nodo de ROS
        rospy.init_node('teleop_coord')

        # Crear un publicador para enviar comandos de movimiento
        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

        # Crear un objeto Twist para almacenar los comandos de movimiento
        self.cmd_vel = Twist()

        # Establecer la velocidad lineal y angular inicial en cero
        self.cmd_vel.linear.x = 0.0
        self.cmd_vel.angular.z = 0.0

    def move_to_coordinates(self, target_x, target_y):
        # Obtener la posición actual del objeto
        current_x = 0.0  # Reemplazar con la posición actual del objeto en el eje X
        current_y = 0.0  # Reemplazar con la posición actual del objeto en el eje Y

        # Calcular la distancia y el ángulo hacia el objetivo
        distance = sqrt((target_x - current_x)**2 + (target_y - current_y)**2)
        angle = atan2(target_y - current_y, target_x - current_x)

        # Definir las velocidades lineal y angular
        linear_speed = 0.5  # Ajustar la velocidad lineal según se desee
        angular_speed = 0.5  # Ajustar la velocidad angular según se desee

        # Calcular las velocidades lineal y angular para alcanzar el objetivo
        self.cmd_vel.linear.x = linear_speed * distance
        self.cmd_vel.angular.z = angular_speed * angle

        # Publicar el comando de movimiento
        self.cmd_vel_pub.publish(self.cmd_vel)

    def run(self):
        # Ejecutar el bucle principal
        while not rospy.is_shutdown():
            # Leer las coordenadas objetivo desde el usuario
            target_x = float(input("Ingrese la coordenada X objetivo: "))
            target_y = float(input("Ingrese la coordenada Y objetivo: "))

            # Mover el objeto a las coordenadas objetivo
            self.move_to_coordinates(target_x, target_y)

        # Detener el objeto antes de finalizar el programa
        self.cmd_vel.linear.x = 0.0
        self.cmd_vel.angular.z = 0.0
        self.cmd_vel_pub.publish(self.cmd_vel)

if __name__ == '__main__':
    try:
        teleop = TeleopCoord()
        teleop.run()
    except rospy.ROSInterruptException:
        pass
