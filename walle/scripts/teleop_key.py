#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Twist
import sys, select, termios, tty

class TeleopKey(object):
    def __init__(self):
        # Inicializar el nodo de ROS
        rospy.init_node('teleop_key')

        # Crear un publicador para enviar comandos de movimiento
        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

        # Crear un objeto Twist para almacenar los comandos de movimiento
        self.cmd_vel = Twist()

        # Establecer la velocidad lineal y angular inicial en cero
        self.cmd_vel.linear.x = 0.0
        self.cmd_vel.angular.z = 0.0

        # Configurar la lectura del teclado sin necesidad de presionar Enter
        self.settings = termios.tcgetattr(sys.stdin)
        tty.setcbreak(sys.stdin.fileno())

        # Imprimir la guía de las teclas que se pueden usar
        print("Control del movimiento del robot:")
        print("  'w' - Avanzar hacia adelante")
        print("  's' - Retroceder")
        print("  'd' - Girar a la izquierda")
        print("  'a' - Girar a la derecha")
        print("  'q' - Salir")

    def getKey(self):
        # Leer la entrada del teclado de manera no bloqueante
        if select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
            return sys.stdin.read(1)
        else:
            return ''

    def run(self):
        # Ejecutar el bucle principal
        while not rospy.is_shutdown():
            # Leer la entrada del teclado
            key = self.getKey()

            # Realizar la acción correspondiente según la tecla ingresada
            if key == 'w':
                
                self.cmd_vel.linear.x = 0
                self.cmd_vel.angular.z = 155
                self.cmd_vel_pub.publish(self.cmd_vel)

            elif key == 's':
                self.cmd_vel.linear.x = 1
                self.cmd_vel.angular.z = 155
                self.cmd_vel_pub.publish(self.cmd_vel)
            elif key == 'd':
                self.cmd_vel.linear.x = 2
                self.cmd_vel.angular.z = 155
                self.cmd_vel_pub.publish(self.cmd_vel)
            elif key == 'a':
                self.cmd_vel.linear.x = 3
                self.cmd_vel.angular.z = 155
                self.cmd_vel_pub.publish(self.cmd_vel)
            elif key == 'q':
                break
            else:
                # Detener el objeto cuando no se está presionando ninguna tecla
                self.cmd_vel.linear.x = 4
                self.cmd_vel.angular.z = 155
                self.cmd_vel_pub.publish(self.cmd_vel)

            # Publicar el comando de movimiento
            
            self.cmd_vel_pub.publish(self.cmd_vel)

            # Esperar un corto periodo de tiempo para evitar la repetición rápida de comandos
            rospy.sleep(0.1)

        # Detener el objeto antes de finalizar el programa
        self.cmd_vel.linear.x = 4
        self.cmd_vel.angular.z = 155

        self.cmd_vel_pub.publish(self.cmd_vel)

        # Restaurar la configuración del teclado
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.settings)

if __name__ == '__main__':
    try:
        teleop = TeleopKey()
        teleop.run()
    except rospy.ROSInterruptException:
        pass
