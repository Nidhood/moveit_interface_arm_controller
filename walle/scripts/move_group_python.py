import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

def plan_and_execute_trajectory(joint_states):
    # Crear un objeto JointTrajectory para la trayectoria
    trajectory = JointTrajectory()

    # Establecer los nombres de las articulaciones
    trajectory.joint_names = ['servo1', 'servo2', 'servo3', 'servo4', 'servo5']

    # Crear un objeto JointTrajectoryPoint para el punto de la trayectoria
    point = JointTrajectoryPoint()

    # Establecer los valores de las articulaciones del punto de la trayectoria
    point.positions = joint_states.position

    # Establecer el tiempo de duración del punto de la trayectoria (en segundos)
    point.time_from_start = rospy.Duration(1.0)  # Ajusta el tiempo según sea necesario

    # Agregar el punto de la trayectoria a la lista de puntos
    trajectory.points.append(point)

    # Publicar la trayectoria en el tópico /arm_controller/command
    trajectory_publisher.publish(trajectory)

def joint_states_callback(data):
    # Llamar a la función para planificar y ejecutar la trayectoria
    plan_and_execute_trajectory(data)

if __name__ == '__main__':
    # Inicializar el nodo de ROS
    rospy.init_node('trajectory_planning')

    # Crear un objeto Publisher para publicar la trayectoria planificada
    trajectory_publisher = rospy.Publisher('/arm_controller/command', JointTrajectory, queue_size=10)

    # Crear un objeto Subscriber para suscribirse a los mensajes de JointState
    joint_states_subscriber = rospy.Subscriber('/arm_controller', JointState, joint_states_callback)

    # Mantener el programa en ejecución
    rospy.spin()
