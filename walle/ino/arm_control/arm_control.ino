#if (ARDUINO >= 100)
 #include <Arduino.h>
#else
 #include <WProgram.h>
#endif

#include <Servo.h> 
#include <ros.h>
#include <sensor_msgs/JointState.h>

Servo servoMotor1;
Servo servoMotor2;
Servo servoMotor3;
Servo servoMotor4;
Servo servoMotor5;
Servo servoClockwise;
Servo servoCounterClockwise;


ros::NodeHandle  nh;

void joinStateCallback(const sensor_msgs::JointState& msg){ 
  if(msg.position[0] == 1){
    servoMotor1.write(50);
  } else if(msg.position[0] == 0){
    servoMotor1.write(0);
  }
  servoMotor2.write(msg.position[1]);
  servoMotor3.write(msg.position[2]);
  servoMotor4.write(msg.position[3]);
  servoMotor5.write(msg.position[4]);
  moveBase(msg.position[5]);
}

ros::Subscriber<sensor_msgs::JointState> jointStateSubs("/arm_controller", joinStateCallback);


void setup(){
  nh.initNode();
  nh.subscribe(jointStateSubs);
  servoMotor1.attach(7);
  servoMotor2.attach(6);
  // Conect each Servo interfaces:
  servoMotor3.attach(5);
  servoMotor4.attach(4);
  servoMotor5.attach(3);
  servoClockwise.attach(2);
  servoCounterClockwise.attach(31);
}

void loop(){
  nh.spinOnce(); // Receive messages
  delay(1);
}


// Move of the base:
void moveBase(float value){

  // Move servo in clockwise:
  servoClockwise.write(value);

  // Move servo in counter clockwise:
  servoCounterClockwise.write(180 - value);
}