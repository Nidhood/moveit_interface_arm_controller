// Generated by gencpp from file walle/head_call.msg
// DO NOT EDIT!


#ifndef WALLE_MESSAGE_HEAD_CALL_H
#define WALLE_MESSAGE_HEAD_CALL_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace walle
{
template <class ContainerAllocator>
struct head_call_
{
  typedef head_call_<ContainerAllocator> Type;

  head_call_()
    : head()  {
    }
  head_call_(const ContainerAllocator& _alloc)
    : head(_alloc)  {
  (void)_alloc;
    }

//std::string

   typedef std::vector<uint16_t, typename std::allocator_traits<ContainerAllocator>::template rebind_alloc<uint16_t>> _head_type;
  _head_type head;





  typedef boost::shared_ptr< ::walle::head_call_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::walle::head_call_<ContainerAllocator> const> ConstPtr;

}; // struct head_call_

typedef ::walle::head_call_<std::allocator<void> > head_call;

typedef boost::shared_ptr< ::walle::head_call > head_callPtr;
typedef boost::shared_ptr< ::walle::head_call const> head_callConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::walle::head_call_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::walle::head_call_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::walle::head_call_<ContainerAllocator1> & lhs, const ::walle::head_call_<ContainerAllocator2> & rhs)
{
  return lhs.head == rhs.head;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::walle::head_call_<ContainerAllocator1> & lhs, const ::walle::head_call_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace walle

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::walle::head_call_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::walle::head_call_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::walle::head_call_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::walle::head_call_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::walle::head_call_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::walle::head_call_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::walle::head_call_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ec126200807c2be7b96837b57db6afa3";
  }

  static const char* value(const ::walle::head_call_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xec126200807c2be7ULL;
  static const uint64_t static_value2 = 0xb96837b57db6afa3ULL;
};

template<class ContainerAllocator>
struct DataType< ::walle::head_call_<ContainerAllocator> >
{
  static const char* value()
  {
    return "walle/head_call";
  }

  static const char* value(const ::walle::head_call_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::walle::head_call_<ContainerAllocator> >
{
  static const char* value()
  {
    return "uint16[] head\n"
;
  }

  static const char* value(const ::walle::head_call_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::walle::head_call_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.head);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct head_call_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::walle::head_call_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::walle::head_call_<ContainerAllocator>& v)
  {
    s << indent << "head[]" << std::endl;
    for (size_t i = 0; i < v.head.size(); ++i)
    {
      s << indent << "  head[" << i << "]: ";
      Printer<uint16_t>::stream(s, indent + "  ", v.head[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // WALLE_MESSAGE_HEAD_CALL_H
