#include <Wire.h>
#include <ros.h>
#include <geometry_msgs/Quaternion.h>

ros::NodeHandle nh;
geometry_msgs::Quaternion gyro_msg;
ros::Publisher gyro_pub("gyro", &gyro_msg);

const int MPU_addr = 0x68; // Dirección I2C del MPU6050

const int filterSize = 10; // Tamaño del filtro de media móvil
int16_t gx_offset, gy_offset, gz_offset; // Valores de compensación del giroscopio
int16_t gx_filtered, gy_filtered, gz_filtered; // Valores filtrados del giroscopio
int16_t gx_raw, gy_raw, gz_raw; // Valores brutos del giroscopio
int16_t gx_samples[filterSize], gy_samples[filterSize], gz_samples[filterSize]; // Almacenamiento de muestras

int16_t gx_initial = 0, gy_initial = 0, gz_initial = 0; // Posición inicial del giroscopio
int16_t gx_current, gy_current, gz_current; // Posición actual del giroscopio

void setup()
{
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B); // PWR_MGMT_1 registro
  Wire.write(0);    // Desactivar el modo de suspensión (activar el MPU6050)
  Wire.endTransmission(true);

  nh.initNode();
  nh.advertise(gyro_pub);

  // Inicializar los valores del filtro y compensación
  for (int i = 0; i < filterSize; i++)
  {
    gx_samples[i] = 0;
    gy_samples[i] = 0;
    gz_samples[i] = 0;
  }
  gx_offset = 0;
  gy_offset = 0;
  gz_offset = 0;
}

void loop()
{
  readGyroValues();
  applyOffset();
  filterGyroValues();
  publishGyroValues();

  nh.spinOnce();

  delay(10);
}

void readGyroValues()
{
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x43); // Dirección del primer registro de giroscopio
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr, 6, true); // Leer 6 bytes del MPU6050

  gx_raw = Wire.read() << 8 | Wire.read();
  gy_raw = Wire.read() << 8 | Wire.read();
  gz_raw = Wire.read() << 8 | Wire.read();
}

void applyOffset()
{
  gx_current = gx_raw - gx_offset;
  gy_current = gy_raw - gy_offset;
  gz_current = gz_raw - gz_offset;
}

void filterGyroValues()
{
  // Desplazar las muestras en el buffer hacia la derecha
  for (int i = filterSize - 1; i > 0; i--)
  {
    gx_samples[i] = gx_samples[i - 1];
    gy_samples[i] = gy_samples[i - 1];
    gz_samples[i] = gz_samples[i - 1];
  }

  // Agregar la nueva muestra al principio del buffer
  gx_samples[0] = gx_current;
  gy_samples[0] = gy_current;
  gz_samples[0] = gz_current;

  gx_filtered = 0;
  gy_filtered = 0;
  gz_filtered = 0;

  // Calcular el promedio de las muestras en el filtro
  for (int i = 0; i < filterSize; i++)
  {
    gx_filtered += gx_samples[i];
    gy_filtered += gy_samples[i];
    gz_filtered += gz_samples[i];
  }

  gx_filtered /= filterSize;
  gy_filtered /= filterSize;
  gz_filtered /= filterSize;
}

void publishGyroValues()
{
  gyro_msg.x = gx_filtered - gx_initial;
  gyro_msg.y = gy_filtered - gy_initial;
  gyro_msg.z = gz_filtered - gz_initial;
  gyro_pub.publish(&gyro_msg);
}

